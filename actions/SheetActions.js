export const ADD_SHEETS = 'ADD_SHEETS';

export const addSheetsActionCreator = (sheets) => {
    return {
        type: ADD_SHEETS,
        payload: {
            sheets: sheets,
            fetching: false
        }
    }
};