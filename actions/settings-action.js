import * as actionTypes from '../constants/action-types';

export const editProfileAction = (user) => {
    return {
        type: actionTypes.EDIT_PROFILE,
        payload: user
    };
}
