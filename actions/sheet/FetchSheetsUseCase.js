import { fetchSheets } from '../../data/ExpenseTrackerApi';
import {addSheetsActionCreator} from "../SheetActions";
import {showLoadingActionCreator} from "../UiActions";

export const fetchSheetsUseCase = () => {
    return (dispatch) => {
        dispatch(showLoadingActionCreator(true));

        fetchSheets()
            .then(res => {
                console.log(`Received ${res.data.sheets.length} sheets`);
                console.log(`Received ${JSON.stringify(res)}`);
                dispatch(addSheetsActionCreator(res.data.sheets));
                dispatch(showLoadingActionCreator(false));
            })
            .catch(err => {
                console.log(err);
                dispatch(showLoadingActionCreator(false));
            });
    }
};