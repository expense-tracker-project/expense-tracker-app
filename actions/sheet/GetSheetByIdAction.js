import * as actionTypes from '../../constants/action-types';
import { fetchSheets } from '../../data/ExpenseTrackerApi';

export const getSheetByIdAction = (sheetId) => {
    return (dispatch, getState) => {
        const { sheets } = getState().sheets;
        const sheet = sheets.find(item => item._id === sheetId);

        if (sheet) {
            dispatch({

            })
        }
        dispatch({
            type: actionTypes.QUERY_SHEETS
        });

        fetchSheets()
            .then(res => {
                console.log(`Received ${JSON.stringify(res)} sheets`);
                console.log(`Received ${res.data.sheets.length} sheets`);
                const sheets = res.data.sheets;
                dispatch({
                    type: actionTypes.QUERY_SHEETS_SUCCESS,
                    sheets: sheets
                })
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: actionTypes.QUERY_SHEETS_ERROR, message: 'An error occurred when querying sheets'
                })
            });
    }
};