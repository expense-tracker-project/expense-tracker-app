import * as actionTypes from '../constants/action-types';

export const toggleEditAction = () => {
    return {
        type: actionTypes.TOGGLE_EDIT,
    };
}

export const toggleModalAction = () => {
    return {
        type: actionTypes.TOGGLE_MODAL
    }
}

export const toggleAuthFormAction = (isLogin) => {
    if (isLogin) {
        return {type: actionTypes.SHOW_LOGIN_FORM}
    } else {
        return {type: actionTypes.SHOW_SIGNUP_FORM}
    }
}
