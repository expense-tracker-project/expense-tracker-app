import * as actionTypes from '../constants/action-types';
import {signup} from '../data/ExpenseTrackerApi';

export const userSignupAction = (email, password, username) => {

    return (dispatch) => {

        //change isLoading state
        dispatch({
            type: actionTypes.USER_SIGNUP_REQUEST
        })

        signup(email, password, username)
            .then(res => {
                const user = {
                    user: res.data.signup.user,
                    token: res.data.signup.token
                }
                dispatch({
                    type: actionTypes.USER_SIGNUP_SUCCESS, user
                })

            })
            .catch(err => {
                dispatch({
                    type: actionTypes.USER_SIGNUP_ERROR,
                    message: 'An error occurred'
                })
            });
    }

}
