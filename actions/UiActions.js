export const SHOW_LOADING = "@uiRoute/showLoading";

export const showLoadingActionCreator = (showLoading) => {
    return {
        type: SHOW_LOADING,
        payload: {
            showLoading: showLoading
        }
    }
};