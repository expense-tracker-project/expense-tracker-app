import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';

const client = new ApolloClient({
    uri: 'http://192.168.0.110:4000/graphql'
})

export const signup = (email, password, username) => {
    return client.mutate({
        mutation: gql `
            mutation createUser($email: String!, $password: String!, $name: String) {
                signup(email: $email, password: $password, name:$name) {
                    token
                    user {
                        _id
                        email
                    }
                }
            }
        `,
        variables: { email: email, password: password, name: username }
    })
};

export const fetchSheets = () => {
    return client.query({
        query: gql `
            query getSheets {
	            sheets {
	                _id
		            name
		            createdAt
		            lastModifiedAt
		            users {
			            _id
			            displayName
		            }
		            expenses {
		                _id
		                name
		                cost
		            }
	            }
            }
        `,
        context: {
            headers: {
                Authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YmJmYTU2NjMyNjYzNTMzZmM2MDUwZDMiLCJpYXQiOjE1MzkyODYzNzR9.M1u5KqQ1fufUw66QzpLqoas_fLpvKzyLk0MYDGdVE6w"
            }
        }
    })
};