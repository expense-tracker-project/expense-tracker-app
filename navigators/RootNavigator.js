import React from "react"
import SheetsScreen from "../containers/HomeScreen"
import CreateSheetScreen from "../containers/CreateSheetScreen"
import GeneralSettingsScreen from "../containers/GeneralSettingsScreen"
import ProfileSettingsScreen from "../containers/ProfileSettingsScreen"
import PaymentSettingsScreen from "../containers/PaymentSettingsScreen"
import NotificationScreen from "../containers/NotificationScreen"
import SettingsScreen from "../containers/SettingsScreen"
import { SimpleLineIcons } from "@expo/vector-icons"
import {createBottomTabNavigator, createStackNavigator} from "react-navigation"
import Ripple from "react-native-material-ripple"
import StartScreen from "../containers/StartScreen"
import AuthenticationScreen from "../containers/AuthenticationScreen"
import MemberScreen from "../containers/MemberScreen"
import MemberDetailScreen from "../containers/MemberDetailScreen"
import BudgetScreen from "../containers/BudgetScreen"
import SheetDetailScreen from '../containers/sheet/SheetDetailScreen'
import ExpenseDetailScreen from '../containers/ExpenseDetailScreen'
import SettingSheetDetailScreen from '../containers/SettingSheetDetailScreen'
import AddMemberScreen from '../containers/AddMemberScreen'
import CurrencySettingsScreen from '../containers/CurrencySettingsScreen'

const defaultHeaderStyle = {
    elevation: 0, // remove shadow on Android
    shadowOpacity: 0, // remove shadow on iOS
    backgroundColor: '#FFF',
    borderBottomWidth: 0
}

const HomeNavigator = createStackNavigator(
    {
        Sheet: {
            screen: SheetsScreen,
        },
        Budget: {
            screen: BudgetScreen,
        },
        SheetDetail: {
            screen: SheetDetailScreen,
        },
        ExpenseDetail: {
            screen: ExpenseDetailScreen,
        },

        SettingSheetDetail: {
            screen: SettingSheetDetailScreen,
        },
        AddMember: {
            screen: AddMemberScreen,
        },
        Member: {
            screen: MemberScreen,
        },
        MemberDetailScreen: {
            screen: MemberDetailScreen,
        },
        Currency: {
            screen: CurrencySettingsScreen
        }
    },
    {
        initialRouteName: 'Sheet',
        headerMode: 'none'
    }
)

const SettingNavigator = createStackNavigator(
    {
        General: {
            screen: GeneralSettingsScreen
        },
        Profile: {
            screen: ProfileSettingsScreen,
            navigationOptions: {
                title: 'Profile Settings',
                headerStyle: {
                    elevation: 0, // remove shadow on Android
                    shadowOpacity: 0, // remove shadow on iOS
                    backgroundColor: 'rgb(101,70,80)',
                    borderBottomWidth: 0
                },
                headerTintColor: '#FFF',
                headerTitleStyle: {
                  fontWeight: '300',
                  fontFamily: 'Avenir',
                  fontSize: 20
                }
            }
        },
        Payment: {
            screen: PaymentSettingsScreen
        },
        Notification: {
            screen: NotificationScreen
        },
        Settings: {
            screen: SettingsScreen
        }
    },
    {
        initialRouteName: 'General',
        headerMode: 'none'
    }
)

HomeNavigator.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }
    return {
        tabBarVisible,
    };
};

SettingNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

const AppNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeNavigator,
            navigationOptions: {
                title: 'Home',
                tabBarIcon: ({ focused, tintColor }) => {
                    const iconName = 'layers';
                    return <SimpleLineIcons name={iconName} size={24} color={tintColor} />
                },
                tabBarButtonComponent: Ripple
            }
        },
        CreateSheet: {
            screen: CreateSheetScreen,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => {
                    const iconName = 'plus';
                    return <SimpleLineIcons name={iconName} size={24} color={tintColor} />
                },
                tabBarButtonComponent: Ripple
            }
        },
        Settings: {
            screen: SettingNavigator,
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => {
                    const iconName = 'user';
                    return <SimpleLineIcons name={iconName} size={24} color={tintColor} />
                },
                tabBarButtonComponent: Ripple
            }
        }
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            showLabel: false
        },
        headerMode: 'none'
    }
)

const RootNavigator = createStackNavigator(
    {
        Start: {
            screen: StartScreen
        },
        Authentication: {
            screen: AuthenticationScreen,
            navigationOptions: {
                headerStyle: {
                    elevation: 0, // remove shadow on Android
                    shadowOpacity: 0, // remove shadow on iOS
                    backgroundColor: '#ED6A5A',
                    borderBottomWidth: 0
                }
            }
        },
        App: {
            screen: AppNavigator
        }
    },
    {
        initialRouteName: 'Start',
        headerMode: 'none'
    }
)

export default RootNavigator


