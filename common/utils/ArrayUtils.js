export const isEmpty = (arr) => {
    return !arr || arr.length === 0
}

export const isNotEmpty = (arr) => {
    return !isEmpty(arr)
}