import { combineReducers } from 'redux'
import settingsReducer from './settings-reducer'
import uiReducer from './UiReducer'
import usersReducer from './users-reducer'
import sheetsReducer from './SheetsReducer'

export default combineReducers({
    usersState: usersReducer,
    sheets: sheetsReducer,
    settingsState: settingsReducer,
    ui: uiReducer,
})
