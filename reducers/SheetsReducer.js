import {updateItemInArray} from "./ReducerUtils";
import {ADD_SHEETS} from "../actions/SheetActions";

const initialState = {
    sheets: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_SHEETS: return storeSheets(state, action);
        default: return state
    }
}

// Store new sheet due to a mechanism that add new and replace old
const storeSheets = (state, action) => {
    const { payload } = action;

    let newSheets = state.sheets ? state.sheets : [];
    payload.sheets.forEach(newItem => {
        if (newSheets.find(item => item._id === newItem._id)) {
            newSheets = updateItemInArray(newSheets, newItem, item => item._id);
        } else {
            newSheets = newSheets.concat(newItem)
        }
    });

    return {
        ...state,
        sheets: newSheets
    }
};