import * as actionTypes from '../constants/action-types';

const initialState = {
    user: null,
    isLoading: false,
    errorMessage: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_SIGNUP_REQUEST:
            return {
                ...state,
                isLoading: true,
            }
        case actionTypes.USER_SIGNUP_SUCCESS:
            return {
                ...state,
                user: action.user,
                isLoading: false
            }
        case actionTypes.USER_SIGNUP_ERROR:
            return {
                ...state,
                errorMessage: action.message,
                isLoading: false
            }
        default:
            return state
    }

}
