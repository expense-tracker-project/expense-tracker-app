export const updateObject = (oldObject, newValues) => {
    // Encapsulate the idea of passing a new object as the first parameter
    // to Object.assign to ensure we correctly copy data instead of mutating
    return Object.assign({}, oldObject, newValues);
};

export const updateItemInArray = (array, newItem, keyExtractor) => {
    return array.map(item => {
        if (keyExtractor(item) !== keyExtractor(newItem)) {
            //Since we only want to update one item, preserve all others as they are now
            return item;
        }
        return updateObject(item, newItem);
    })
};