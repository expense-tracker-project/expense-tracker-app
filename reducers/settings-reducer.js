import * as actionTypes from '../constants/action-types';

const initialState = {
    user: {
        username: 'Kidsukezzz',
        memberType: 'Basic member',
        email: 'longvh@gmail.com',
    }
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.EDIT_PROFILE:
            return {
                ...state,
                user: action.payload.user
            }
        default:
            return state
    }

}
