import * as actionTypes from '../constants/action-types';
import {SHOW_LOADING} from "../actions/UiActions";

const initialState = {
    showLoading: false,
    isEditing: false,
    modalVisible: false,
    showLoginForm: true,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_LOADING: return showLoading(state, action);
        case actionTypes.TOGGLE_EDIT:
            return {
                ...state,
                isEditing: !state.isEditing
            }
        case actionTypes.TOGGLE_MODAL:
            return {
                ...state,
                modalVisible: !state.modalVisible
            }
        case actionTypes.SHOW_LOGIN_FORM:
            return {
                ...state,
                showLoginForm: true
            }
        case actionTypes.SHOW_SIGNUP_FORM:
            return {
                ...state,
                showLoginForm: false
            }
        default:
            return state
    }

}

const showLoading = (state, action) => {
    const { payload } = action;

    return {
        ...state,
        showLoading: payload.showLoading
    }
};