import colors from './colors';

export default {
    boldGreyText: {
        fontWeight: 'bold',
        color: colors.darkGrey,
        marginHorizontal: 10
    }
}