export default {
    primary: '#FB0D6A',
    secondary: '#FAC739',
    darkGrey: '#838383',
    lightGrey: '#A8A8A8',
    white: '#FFFFFF',
    black: '#000000',
    placeholder: '#766A62',
}

