export const TITLE_HOME_SCREEN = 'Home';
export const TITLE_SETTING_SCREEN = 'Settings';
export const TITLE_CREATE_SHEET_SCREEN = 'Add Sheet';
export const TITLE_PROFILE_SCREEN = 'Profile';
export const TITLE_BUDGET_SCREEN = 'Budget';
export const TITLE_MEMBER_SCREEN = 'Members';
export const TITLE_CURRENCY_SETTINGS_SCREEN = 'Currency';
export const TITLE_PROFILE_SETTINGS_SCREEN = 'Profile';


