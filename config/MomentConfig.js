import moment from 'moment'

export default () => {
    moment.updateLocale('en', {
        relativeTime : {
            future: "in %s",
            past: "%s ago",
            s  : '1s',
            ss : '%ds',
            m: "m",
            mm: "%dm",
            h: "1h",
            hh: "%dh",
            d: "1d",
            dd: "%dd"
        }
    })
}