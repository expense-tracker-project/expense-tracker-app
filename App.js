import React from 'react';
import { Font } from 'expo';
import RootNavigator from './navigators/RootNavigator';
import { Provider } from 'react-redux';
import configureStore from './configure-store';
import configureMoment from './config/MomentConfig'
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';

configureMoment();
const store = configureStore();
const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
});


export default class App extends React.Component {

    componentDidMount() {
        Font.loadAsync({
            'tringicons': require('./assets/fonts/tringicons.ttf'),
        });
    }

    render() {
        return (
            <ApolloProvider client={client}>
                <Provider store={store}>
                    <View style={styles.container}>
                        <RootNavigator/>
                    </View>
                </Provider>
            </ApolloProvider>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
});

