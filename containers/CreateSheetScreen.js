import React, { Component } from 'react';
import { StyleSheet, View } from "react-native";
import CreateSheetForm from "../components/CreateSheetForm";
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {toggleEditAction, toggleModalAction} from '../actions/ui-action';
import colors from '../constants/colors';
import { TITLE_CREATE_SHEET_SCREEN } from '../constants/title_screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../components/AppHeader';


class CreateSheetScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sheetType: [
                {
                    label: 'Public',
                    value: 'public'
                },
                {
                    label: 'Private',
                    value: "private",
                }
            ],
            sheetCategory: [
                {
                    value: 'Party',
                },
                {
                    value: 'Travel',
                }
            ],
            pickedDueDate: null,
        }
    }

    componentDidMount() {
        const date = new Date();
        date.setDate(date.getDate() + 5);
        this.setState({ pickedDueDate: moment(date).format('dddd, DD/MM/YYYY') })
    }

    handleSheetTypePicked = sheetType => {
        this.setState({ sheetType })
    };


    handleDatePicked = (date) => {
        const pickedDueDate = moment(date).format('dddd, DD/MM/YYYY');
        this.setState({pickedDueDate});
        this.props.toggleModalAction();
    };

    render() {
        const {sheetType, sheetCategory, pickedDueDate} = this.state
        const {modalVisible, toggleModalAction} = this.props

        return (
            <View style={styles.container}>
                <AppHeader title={TITLE_CREATE_SHEET_SCREEN} />
                <KeyboardAwareScrollView
                    resetScrollToCoords={{ x: 0, y: 0 }}
                >
                    <View style={styles.formContainer}>
                        <CreateSheetForm
                            sheetType={sheetType}
                            sheetCategory={sheetCategory}
                            isDatePickerVisible={modalVisible}
                            toggleDateTimePicker={toggleModalAction}
                            handleDatePicked={this.handleDatePicked}
                            handleSheetTypePicked={this.handleSheetTypePicked}
                            pickedDueDate={pickedDueDate}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flex: 1
    },
    formContainer: {
        padding: 10,
        flex: 1,
        justifyContent: 'center'
    }
})

const mapStateToProps = (store) => {
    return {
        modalVisible: store.ui.modalVisible
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModalAction: bindActionCreators(toggleModalAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSheetScreen);


