import React from 'react';
import { FlatList, View, StyleSheet, TouchableOpacity, Keyboard } from "react-native";
import SearchBar from "../components/SearchBar";
import Currency from "../components/Currency";
import Icon from 'react-native-vector-icons/Feather';
import Fuse from 'fuse.js';
import MyIcon from '../config/icon-font';
import AppHeader from '../components/AppHeader';
import colors from '../constants/colors';
import { TITLE_CURRENCY_SETTINGS_SCREEN } from '../constants/title_screen';

export default class CurrencySettingsScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedCurrency: 'EU',
            currencies: [
                {
                    code: 'EU',
                    name: 'Euro',
                },
                {
                    code: 'US',
                    name: 'US Dollar',
                },
                {
                    code: 'GB',
                    name: 'UK Pound',
                },
                {
                    code: 'VN',
                    name: 'Vietnam Dong',
                },
                {
                    code: 'JP',
                    name: 'Japanese Yen'
                },
                {
                    code: 'CA',
                    name: 'Canadian Dollar'
                },
                {
                    code: 'AU',
                    name: 'Australian Dollar'
                },
                {
                    code: 'CH',
                    name: 'Swiss Franc'
                },
                {
                    code: 'KR',
                    name: 'South Korean Won'
                },

            ],
            isSearching: false,
            searchResult: []
        }
    }

    chooseCurrency = (currencyCode) => {
        this.setState({
            selectedCurrency: currencyCode,
            isSearching: false
        })
        Keyboard.dismiss();
    }

    saveCurrency = () => {
        console.log("Saving Currency");
    }

    searchCurrency = (searchQuery) => {
        const { currencies } = this.state;
        const searchOptions = {
            shouldSort: true,
            threshold: 0.6,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: [
                'code', 'name'
            ]
        }

        const fuse = new Fuse(currencies, searchOptions);
        if (searchQuery.length) {
            this.setState({
                isSearching: true,
                searchResult: fuse.search(searchQuery)
            });
        } else {
            this.setState({ isSearching: false })
        }
    }

    render() {
        const { selectedCurrency, currencies, searchResult, isSearching } = this.state;
        return (
            <View style={styles.container}>
                <AppHeader 
                    title={TITLE_CURRENCY_SETTINGS_SCREEN}
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    rightIcons={
                        <TouchableOpacity onPress={this.saveCurrency}>
                            <MyIcon 
                                name={'tick'}
                                size={18}
                                color={colors.white}
                            />
                        </TouchableOpacity>
                    }/>
                />
                <View style={styles.content}>
                    <SearchBar handleSearching={this.searchCurrency} />
                    <FlatList
                        keyboardShouldPersistTaps={'handled'}
                        data={isSearching ? searchResult : currencies}
                        extraData={this.state}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.chooseCurrency(item.code)}>
                                <View style={styles.currencyRow}>
                                    <Currency
                                        currency={item}
                                    />
                                    {selectedCurrency === item.code ? <Icon name='check' size={24} /> : null}
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item) => `${item.code}`}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#FFF'
    },
    currencyRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
});
