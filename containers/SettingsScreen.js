import React from "react"
import AppText from "../components/AppText";
import {StyleSheet,View} from "react-native";

class SettingsScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <AppText>Here goes settings screen</AppText>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
        backgroundColor: "#FFF",
        alignItems: 'center'
    }
})

export default SettingsScreen
