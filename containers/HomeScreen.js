import React from "react"
import { SectionList, View, StyleSheet, FlatList, RefreshControl, TouchableOpacity, ScrollView } from "react-native"
import Sheet from "../components/sheet/Sheet"
import AppText from "../components/AppText"
import {bindActionCreators} from "redux"
import _ from 'underscore'
import connect from "react-redux/es/connect/connect"
import {fetchSheetsUseCase} from "../actions/sheet/FetchSheetsUseCase"
import moment from 'moment'
import AppHeader from '../components/AppHeader';
import MyIcon from './../config/icon-font.js';
import { TITLE_HOME_SCREEN } from '../constants/title_screen';
import basicCSS from '../constants/basicCSS';
import colors from '../constants/colors';
import SheetList from "../components/sheet/SheetList";

class HomeScreen extends React.Component {

    componentDidMount() {
        const { fetchSheets } = this.props;
        fetchSheets()
    }

    onSheetPress = (sheetId) => {
        const { navigation } = this.props;
        navigation.navigate('SheetDetail', {
            sheetId: sheetId
        });
    };

    render() {
        const { sheets } = this.props;

        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={false}
                    title={TITLE_HOME_SCREEN}
                    rightIcons={
                        <TouchableOpacity>
                            <MyIcon
                                name={'search-interface-symbol'}
                                size={20}
                                color={colors.white} />
                        </TouchableOpacity>
                    }
                />
                {
                    sheets && sheets.length > 0 ? this.renderSheets() : this.renderEmpty()
                }
            </View>
        );
    }

    renderSheets() {
        const { fetchSheets, sheets, showLoading } = this.props;

        let recentSheetsSection = {
            key: '$RecentSheets',
            title: 'Recent sheets',
            renderItem: ({item}) =>
                <SheetList
                    style={styles.marginHorizontal}
                    sheets={item.sheets}
                    horizontal={true}
                    mode={'recently'}
                    onItemPress={this.onSheetPress}/>
            ,
            data: [
                {
                    key: '$RecentSheets',
                    sheets: _.chain(sheets)
                        .sortBy(sheet => moment(sheet.lastModifiedAt, 'MMMM Do YYYY, h:mm:ss a').milliseconds())
                        .first(10)
                        .value()
                }
            ]
        };

        let allSheetsSection = {
            key: '$AllSheets',
            title: 'All sheets',
            renderItem: ({item}) =>
                <SheetList
                    style={styles.marginHorizontal}
                    sheets={item.sheets}
                    onItemPress={this.onSheetPress}/>
            ,
            data: [
                {
                    key: '$AllSheets',
                    sheets: sheets
                }
            ]
        };

        return (
            <SectionList
                renderSectionHeader={({section: {title}}) =>
                    <View style={styles.sectionHeader}>
                        <AppText style={[basicCSS.boldGreyText, styles.sectionHeaderLabel]}>{title}</AppText>
                    </View>
                }
                SectionSeparatorComponent={() => <View style={styles.sectionSeparator} />}
                sections={[
                    recentSheetsSection,
                    allSheetsSection
                ]}
                keyExtractor={(item) => item.key}
                refreshing={showLoading}
                onRefresh={fetchSheets}
            />
        );
    }

    renderEmpty() {
        const { fetchSheets, showLoading } = this.props;

        return (
            <ScrollView
                contentContainerStyle={styles.emptyContentContainer}
                refreshControl={
                    <RefreshControl
                        refreshing={showLoading}
                        onRefresh={fetchSheets}
                    />
                }
            >
                <AppText style={{textAlign: 'center', textAlignVertical: 'center'}}>No sheets found</AppText>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    sectionHeader: {
        flex: 1
    },
    sectionHeaderLabel: {
        marginLeft: 20,
        marginTop: 10,
        marginBottom: 10
    },
    sectionSeparator: {
        height: 10
    },
    emptyContentContainer: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSheets: bindActionCreators(fetchSheetsUseCase, dispatch)
    }
};

const mapStateToProps = (state) => {
    return {
        sheets: state.sheets.sheets,
        showLoading: state.ui.showLoading
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
