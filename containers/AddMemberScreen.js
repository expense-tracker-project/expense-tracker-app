import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import AddMemberForm from '../components/AddMemberForm';
import memberData from '../fixtures/memberData.json';
import colors from '../constants/colors';
import MyIcon from './../config/icon-font.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../components/AppHeader';

const data = memberData.map(user => {
    return { value: user.username }
})

class AddMemberScreen extends React.Component {

    render() {
        const { type } = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    title={`Add ${type} Member`} />
                <KeyboardAwareScrollView 
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    style={styles.formContainer}>
                    <View>
                        <AddMemberForm
                            type={type}
                            memberData={data}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flex: 1
    },
    formContainer: {
        padding: 20,
        flex: 1
    }
})

export default AddMemberScreen;
