import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, TouchableOpacity } from "react-native";
import MemberInfo from "../components/MemberInfo";
import IconButton from "../components/IconButton";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toggleEditAction } from '../actions/ui-action';
import AppHeader from '../components/AppHeader';
import colors from '../constants/colors';
import MyIcon from '../config/icon-font';

class MemberDetailScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <View style={{ flexDirection: 'row' }}>
                    <IconButton name="edit-2" onPress={navigation.getParam('toggleEditMember')} />
                    <IconButton name="trash-2" onPress={navigation.getParam('removeMember')} />
                </View>
            ),
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({
            toggleEditMember: this.props.toggleEditAction,
            removeMember: this.props.navigation.state.params.removeMember
        })
    }

    editMember = (newNickname, newRole, userId) => {
        const { editMember } = this.props.navigation.state.params;
        editMember(newNickname, newRole, userId);
        this.props.toggleEditMember();
    }

    renderHeaderRightIcons = () => {
        return (
            <View style={styles.rightIcons}>
                <TouchableOpacity onPress={this.toggleEditMember}>
                    <MyIcon
                        style={styles.marginRight}
                        name={'edit'}
                        size={20}
                        color={colors.white} />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.props.navigation.state.params.removeMember}>
                    <MyIcon
                        name={'garbage'}
                        size={20}
                        color={colors.white} />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const {member} = this.props.navigation.state.params;
        const {isEditing, toggleEditAction} = this.props;

        return (
            <View style={styles.container}>
                <AppHeader
                    title={member.username}
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    rightIcons={
                        this.renderHeaderRightIcons()
                    } />
                <KeyboardAwareScrollView
                    style={{backgroundColor: '#FFF'}}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                >
                    <View style={styles.content}>
                        <MemberInfo
                            member={member}
                            isEditing={isEditing}
                            toggleEditMember={toggleEditAction}
                            editMember={this.editMember}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    content: {
        flex: 1,
        padding: 20
    },
    rightIcons: {
        flexDirection: 'row'
    },
    marginRight: {
        marginRight: 20
    }
});

const mapStateToProps = (store) => {
    return {
        isEditing: store.ui.isEditing
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleEditAction: bindActionCreators(toggleEditAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemberDetailScreen);
