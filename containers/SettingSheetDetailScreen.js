import React from 'react';
import { View, TouchableOpacity, StyleSheet, Switch } from 'react-native';
import { LinearGradient } from 'expo';
import MyIcon from './../config/icon-font.js';
import colors from '../constants/colors';
import basicCSS from '../constants/basicCSS';
import AppHeader from '../components/AppHeader';
import BasicListItem from '../components/BasicListItem';
import AppText from '../components/AppText';

class SettingSheetDetailScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            notificationOn: true
        }
    }

    handleNavigation = (routeName, params) => {
        const { navigation } = this.props;
        navigation.navigate(routeName, params);
    }

    render() {
        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    title='Settings' />
                <View style={styles.content}>
                    <View style={styles.management}>
                        <AppText style={basicCSS.boldGreyText}>Management</AppText>
                        <BasicListItem
                            left={<AppText>Members</AppText>}
                            right={
                                <MyIcon
                                        name={'arrow-right-sign'}
                                        size={16}
                                        color={colors.black}
                                        style={styles.arrowRight} />
                            }
                            onItemPress={() => this.handleNavigation('Member')}
                        />
                        <BasicListItem
                            left={<AppText>Currency</AppText>}
                            right={
                                <View style={styles.currency}>
                                    <AppText>{`Euro (${'\u20AC'})`}</AppText>
                                    <MyIcon
                                        name={'arrow-right-sign'}
                                        size={16}
                                        color={colors.black}
                                        style={styles.arrowRight} />
                                </View>
                            }
                            onItemPress={() => this.handleNavigation('Currency')}
                        />
                    </View>
                    <View style={styles.others}>
                        <AppText style={basicCSS.boldGreyText}>Others</AppText>
                        <BasicListItem
                            left={<AppText>Push notifications</AppText>}
                            right={
                                <Switch
                                    onTintColor={colors.primary}
                                    value={this.state.notificationOn}
                                    onValueChange={() => this.setState({ notificationOn: !this.state.notificationOn })} />
                            } />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        paddingTop: 20,
        paddingHorizontal: 20
    },
    others: {
        marginTop: 20
    },
    currency: {
        flexDirection: 'row'
    },
    arrowRight: {
        paddingLeft: 10
    }
})

export default SettingSheetDetailScreen;
