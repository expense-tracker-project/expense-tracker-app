import React from 'react';
import {StyleSheet, View} from 'react-native';
import AppText from '../components/AppText';
import AuthenticationForm from '../components/AuthenticationForm';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class AuthenticationScreen extends React.Component {

    static navigationOptions = {
        headerStyle:{
            backgroundColor: '#ED6A5A',
            borderBottomWidth: 0,
        },
        headerTintColor: "#fff",
    }

    constructor(props) {
        super(props);
    }

    handleNavigation = (routeName, params) => {
        const { navigation } = this.props;
        navigation.navigate(routeName, params);
    }

    render() {
        return (
            <KeyboardAwareScrollView
                style={{ backgroundColor: '#ED6A5A', flex: 1 }}
                resetScrollToCoords={{ x: 0, y: 0 }}
            >
                <View style={styles.container}>
                    <View style={{marginBottom: 50}}>
                        <View style={{width: 150, height: 150, borderRadius: 75, backgroundColor: "#fff", justifyContent:"center"}}>
                            <AppText style={{textAlign: "center", fontSize: 25}}>App Logo</AppText>
                        </View>
                    </View>
                    <AuthenticationForm onNavigation={this.handleNavigation}/>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        marginTop: 60
    },
})

export default AuthenticationScreen;
