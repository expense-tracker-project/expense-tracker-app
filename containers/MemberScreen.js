import React, { Component } from 'react';
import {StyleSheet, View, SectionList, Alert, TouchableOpacity} from 'react-native';
import AppText from "../components/AppText";
import MemberListItem from "../components/MemberListItem";
import IconButton from "../components/IconButton";
import AppHeader from '../components/AppHeader';
import colors from '../constants/colors';
import MyIcon from '../config/icon-font';
import { TITLE_MEMBER_SCREEN } from '../constants/title_screen';

class MemberScreen extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <IconButton name="user-plus" onPress={navigation.getParam('addMember')}/>
            ),
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            hosts: {
                title: 'HOSTS',
                role: 'Host',
                data: [
                    {id: 1, username: 'longv', nickname: 'Kidsukezzz', role: 'Host'}
                ]
            },

            members: {
                title: 'MEMBERS',
                role: 'Member',
                data: [
                    {id: 2, username: 'phuch', nickname: 'Phoccute', role: 'Member'},
                    {id: 3, username : 'tuand', nickname: 'Lame', role: 'Member'},
                    {id: 4, username: 'khaph', nickname: 'Ba trum', role: 'Member'},
                    {id: 5, username: 'tring', nickname: 'Truc', role: 'Member'},
                ]
            }
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({
            addMember: this.addMember
        })
    }

    handleNavigation = (routeName, params) => {
        const { navigation } = this.props
        navigation.navigate(routeName, params)
    }

    editMember = (newNickname,newRole,userId) => {
        const hosts = Object.assign({}, this.state.hosts);
        const members = Object.assign({}, this.state.members);

        hosts.data.concat(members.data)
            .forEach(member => {
                if (member.id === userId) {
                    member.nickname = newNickname;
                    member.role = newRole;
                }
            })

        this.handleRoleChange(hosts,members);
        this.handleRoleChange(members,hosts);

        this.setState({hosts,members});
    }

    handleRoleChange = (originalRole, newRole) => {
        originalRole.data.map((member,index) => {
            if(member.role.toLowerCase() !== originalRole.role.toLowerCase()) {
                newRole.data.push(originalRole.data[index]);
                originalRole.data.splice(index, 1);
            }
        })
    }

    addMember = () => {
        console.log('Add user')
    }

    removeMember = () => {
        Alert.alert(
            'Remove member',
            'Are you sure to remove this member from this sheet?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }

    render() {
        const {hosts, members} = this.state
        return (
            <View style={styles.container}>
                <AppHeader 
                    title={TITLE_MEMBER_SCREEN}
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    rightIcons={
                        <TouchableOpacity onPress={this.addMember}>
                            <MyIcon 
                                name={'add-user'}
                                size={20}
                                color={colors.white}
                            />
                        </TouchableOpacity>
                    }/>
                <View style={styles.memberList}>
                    <SectionList
                        renderSectionHeader={({section: {title}}) => (
                            <AppText style={{fontWeight: 'bold'}}>{title}</AppText>
                        )}
                        sections={[hosts, members]}
                        renderItem={({item, index}) =>
                            <MemberListItem
                                key={index}
                                member={item}
                                editMember={this.editMember}
                                removeMember={this.removeMember}
                                onPress={this.handleNavigation}
                            />
                        }
                        SectionSeparatorComponent={() => <View style={styles.sectionSeparator}/>}
                        keyExtractor={(item, index) => item + index}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    sectionSeparator: {
        height: 10,
    },
    memberList: {
        flex: 1,
        padding: 20
    }
});



export default MemberScreen;
