import React from "react"
import AppText from "../components/AppText";
import {StyleSheet,View,TouchableOpacity, FlatList, ScrollView} from "react-native"
import { Feather } from "@expo/vector-icons"
import UserAvatar from "../components/UserAvatar"
import { connect } from 'react-redux'
import { TITLE_SETTING_SCREEN } from '../constants/title_screen';
import AppHeader from '../components/AppHeader';

class GeneralSettingsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.settingItems = [
            'Profile',
            'Notification',
            'Payment',
            'Settings',
            'About us',
            'FAQ'
        ]
    }

    handleNavigation = (routeName, params) => {
        const { navigation } = this.props;
        navigation.navigate(routeName, params);
    }

    render() {
        const {user} = this.props
        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={false}
                    title={TITLE_SETTING_SCREEN}/>
                <ScrollView
                    contentContainerStyle={styles.content}
                >
                    <UserAvatar username={user.username} memberType={user.memberType}/>
                    <View>
                        <FlatList
                            data={this.settingItems}
                            renderItem={({item, index}) =>
                                <TouchableOpacity onPress={() => this.handleNavigation(item,{user})}>
                                    <View
                                        style={[styles.settingRow, index===this.settingItems.length-1 && styles.noBorderRow]}
                                    >
                                        <AppText style={styles.settingRowLabel}>{item}</AppText>
                                        <Feather name='chevron-right' size={24} color="#FFFFFF"/>
                                    </View>
                                </TouchableOpacity>
                            }
                            keyExtractor={(item) => `${item}`}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgb(101,70,80)"
    },
    content: {
        flexGrow: 1,
        paddingTop: 50,
        justifyContent: 'space-around'
    },
    settingRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 5,
        borderBottomWidth: 0.7,
        borderBottomColor: '#BBB5B5',
        marginLeft: 10,
        marginRight: 10
    },
    noBorderRow: {
        borderBottomWidth: 0
    },
    settingRowLabel: {
        fontSize: 20,
        color: 'white'
    }
})

const mapStateToProps = (store) => {
    return {
        user: store.settingsState.user
    }
}

export default connect(mapStateToProps)(GeneralSettingsScreen);

