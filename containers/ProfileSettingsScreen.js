import React from "react"
import {StyleSheet,View, TouchableOpacity} from "react-native"
import UserAvatar from "../components/UserAvatar"
import UserProfile from "../components/UserProfile"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import IconButton from "../components/IconButton";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editProfileAction } from '../actions/settings-action.js';
import { toggleEditAction } from '../actions/ui-action';
import { TITLE_PROFILE_SETTINGS_SCREEN } from '../constants/title_screen';
import AppHeader from '../components/AppHeader';
import MyIcon from '../config/icon-font';
import colors from '../constants/colors';

class ProfileSettingsScreen extends React.Component {

    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <IconButton name="edit-2" color="#FFF" onPress={navigation.getParam('toggleEditProfile')}/>
            ),
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({
            toggleEditProfile: this.props.toggleEditAction
        })
    }

    editProfile = (newEmail,newUsername) => {
        const user = Object.assign({}, this.props.navigation.state.params.user)
        user.email = newEmail;
        user.username = newUsername;
        this.props.editProfileAction({user})
        this.props.toggleEditAction()
    }

    render() {
        const {user, isEditing, toggleEditAction} = this.props

        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    title={TITLE_PROFILE_SETTINGS_SCREEN}
                    rightIcons={
                        <TouchableOpacity onPress={this.toggleEditProfile}>
                            <MyIcon
                                name={'edit'}
                                size={20}
                                color={colors.white} />
                        </TouchableOpacity>
                    }/>
                <KeyboardAwareScrollView
                    style={{backgroundColor: 'rgb(101,70,80)'}}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                >
                    <View style={styles.content}>
                        <UserAvatar
                            mode="horizontal"
                            username={user.username}
                            memberType={user.memberType}
                            style={styles.userAvatar}
                            usernameStyle={styles.textColor}
                            userTypeStyle={styles.textColor}
                        />
                        <UserProfile
                            user={user}
                            isEditing={isEditing}
                            toggleEdit={toggleEditAction}
                            editProfile={this.editProfile}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(101,70,80)'
    },
    content: {
        flex: 1,
        backgroundColor: 'rgb(101,70,80)',
        paddingTop: 30,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    },
    textColor: {
        color: "#FFF"
    },
    userAvatar: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: 'rgb(166,127,142)',
    }
})

const mapStateToProps = (store) => {
    return {
        user: store.settingsState.user,
        isEditing: store.ui.isEditing
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editProfileAction: bindActionCreators(editProfileAction, dispatch),
        toggleEditAction: bindActionCreators(toggleEditAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettingsScreen);
