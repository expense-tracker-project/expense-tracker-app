import React from 'react';
import {
    View,
    TouchableOpacity,
    SectionList,
    Platform,
    StyleSheet,
    ActionSheetIOS
} from 'react-native';
import _ from 'lodash';
import showPopupMenu from 'react-native-popup-menu-android'
import MyIcon from './../config/icon-font.js';
import colors from '../constants/colors';
import basicCSS from '../constants/basicCSS';
import AppText from '../components/AppText';
import PayerDetailListItem from '../components/PayerDetailListItem';
import ReturnerListItem from '../components/ReturnerListItem';
import expenseData from '../fixtures/expenseData.json';
import AppHeader from '../components/AppHeader';

const data = _.keyBy(expenseData, 'id');

const BUTTONSiOS = [
    'Add paid members',
    'Add returned members',
    'Cancel'
];

const BUTTONSandroid = [
    { label: 'Add paid members', index: 0 },
    { label: 'Add returned members', index: 1 }
];

const CANCEL_INDEX = 2;

class ExpenseDetailScreen extends React.Component {

    moreButton = null

    _refMoreButton = el => this.moreButton = el

    onAddUserOptions = () => {
        if (Platform.OS === 'ios') {
            return ActionSheetIOS.showActionSheetWithOptions({
                options: BUTTONSiOS,
                title: 'Add members',
                cancelButtonIndex: CANCEL_INDEX
            },
                this.handleItemSelect
            );
        } else {
            return showPopupMenu(
                BUTTONSandroid,
                ({ index }) => this.handleItemSelect(index),
                this.moreButton
            );
        }
    }

    handleItemSelect = (index) => {
        const { navigation } = this.props;

        switch (index) {
            case 0:
                navigation.navigate("AddMember", { type: 'Paid' })
                break;
            case 1:
                navigation.navigate("AddMember", { type: 'Returned' })
                break;
            default:
                break;
        }
    }

    renderItems = (item, index, section) => {
        switch (section.title) {
            case 'Paid By':
                if (index === section.data.length - 1) {
                    return (
                        <View>
                            <PayerDetailListItem {...item} />
                            {this.renderTotalAmount()}
                        </View>
                    );
                }
                return <PayerDetailListItem {...item} />
            case 'Returned By':
                return <ReturnerListItem returner={item} />
            default:
                return null;
        }
    }

    renderTotalAmount = () => {
        const expense = data[this.props.navigation.state.params.id];
        return (
            <View style={styles.total}>
                <AppText style={[basicCSS.boldGreyText, styles.totalText]}> Total </AppText>
                <AppText style={styles.amount}> {`${'\u20AC'}${expense.totalAmount}`} </AppText>
            </View>
        )
    }

    render() {
        const expense = data[this.props.navigation.state.params.id];
        return (
            <View style={styles.container}>
                <AppHeader
                    hasBackButton={true}
                    navigation={this.props.navigation}
                    title={expense.name}
                    rightIcons={
                        <TouchableOpacity
                            onPress={this.onAddUserOptions}
                            ref={this._refMoreButton}>
                            <MyIcon
                                name={'add-user'}
                                size={20}
                                color={colors.white} />
                        </TouchableOpacity>
                    }/>
                <View style={styles.content}>
                    <SectionList
                        renderItem={({ item, index, section }) => this.renderItems(item, index, section)}
                        renderSectionHeader={({ section: { title } }) => (
                            <AppText style={[basicCSS.boldGreyText, styles.headers]}>{title}</AppText>
                        )}
                        sections={[
                            { title: 'Paid By', data: expense.payers },
                            { title: 'Returned By', data: expense.returners },
                        ]}
                        keyExtractor={(item, index) => `${item.id}`}
                        stickySectionHeadersEnabled={false}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 20
    },
    headers: {
        marginVertical: 10
    },
    paidBy: {
        flex: 1
    },
    returnedBy: {
        flex: 1
    },
    totalText: {
        fontSize: 20,
        right: -5
    },
    amount: {
        fontWeight: 'bold',
        color: colors.primary,
        fontSize: 25
    },
    total: {
        marginTop: 5,
        alignSelf: 'flex-end',
        alignItems: 'flex-end'
    }
})

export default ExpenseDetailScreen;