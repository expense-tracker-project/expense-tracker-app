import React from 'react';
import {StyleSheet, View } from 'react-native';
import SheetCodeModal from '../components/SheetCodeModal';
import {Button} from 'react-native-elements';
import AppText from '../components/AppText';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {toggleModalAction} from '../actions/ui-action';

class StartScreen extends React.Component {

    static navigationOptions = {
        header: null,
        headerStyle:{
            backgroundColor: 'transparent',
            borderBottomWidth: 0
        },
    }

    render() {
        const {modalVisible, toggleModalAction, navigation} = this.props;
        return (
            <View style={styles.container}>
                <AppText style={styles.header}>
                    Welcome to Expense Tracker
                </AppText>
                <View style={styles.subContainer}>
                    <Button
                        containerViewStyle={styles.buttonContainer}
                        title="Join as anonymous"
                        buttonStyle={styles.button}
                        textStyle={styles.buttonTitle}
                        onPress={toggleModalAction}
                    />
                    <Button
                        containerViewStyle={styles.buttonContainer}
                        title="Join as member"
                        buttonStyle={styles.button}
                        textStyle={styles.buttonTitle}
                        onPress={() => navigation.navigate('Authentication')}
                    />
                </View>
                <SheetCodeModal
                    modalVisible={modalVisible}
                    setModalVisible={toggleModalAction}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ED6A5A",
        justifyContent: "center",
        alignItems: "center"
    },
    subContainer: {
        flex: 1,
        alignItems: "center",
        paddingTop: 100
    },
    header: {
        fontSize: 25,
        marginTop: 150,
        marginBottom: 50,
        color: "#fff",
        fontWeight: "700"
    },
    buttonContainer: {
        width: 300,
        padding: 20,
    },
    buttonTitle: {
        fontSize: 23,
        fontFamily: "Avenir",
        fontWeight: "700",
        color: "#735290"
    },
    button: {
        backgroundColor: "rgba(255, 255, 255, 0.4)",
        borderRadius: 10,
    }

})

const mapStateToProps = (store) => {
    return {
        modalVisible: store.ui.modalVisible
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleModalAction: bindActionCreators(toggleModalAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen);
