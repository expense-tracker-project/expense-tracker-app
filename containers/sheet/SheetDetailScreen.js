import React from 'react';
import { View, StyleSheet } from 'react-native';
import { StackActions } from 'react-navigation';
import SheetDetailHeader from '../../components/SheetDetailHeader';
import BasicListItem from '../../components/BasicListItem';
import ExpenseList from '../../components/expense/ExpenseList';
import colors from '../../constants/colors';
import basicCSS from '../../constants/basicCSS';
import sheetData from '../../fixtures/sheetData.json';
import AppText from '../../components/AppText';
import connect from "react-redux/es/connect/connect";
import _ from 'underscore';

class SheetDetailScreen extends React.Component {

    onExpensePress = (expenseId) => {
        const { navigation } = this.props;
        navigation.navigate('ExpenseDetail', {
            expenseId: expenseId
        });
    };

    render() {
        const { sheet } = this.props;

        return (
            <View style={styles.container}>
                <SheetDetailHeader
                    popAction={() => this.props.navigation.goBack(null)}
                    handleNavigation={this.handleNavigation} />
                <View style={styles.content}>
                    <View style={styles.overallInfo}>
                        <View style={styles.locationAndMembers}>
                            <AppText style={[basicCSS.boldGreyText]}>{sheet.sheetName}</AppText>
                            <AppText style={[basicCSS.boldGreyText]}>{sheet.users.length}</AppText>
                        </View>
                        <View style={styles.budgetAndDeposit}>
                            <BasicListItem
                                left={<AppText>Budget</AppText>}
                                right={<AppText style={styles.amount}>{`${'\u20AC'}100`}</AppText>}
                                onItemPress={() => this.handleNavigation('Budget')}
                            />
                            <BasicListItem
                                left={<AppText>Deposit</AppText>}
                                right={<AppText style={styles.amount}>{`${'\u20AC'}50`}</AppText>} />
                        </View>
                    </View>
                    <View style={styles.expense}>
                        <AppText style={basicCSS.boldGreyText}>Expenses</AppText>
                        <View style={styles.itemList}>
                            <ExpenseList
                                expenses={sheet.expenses}
                                onItemPress={this.onExpensePress} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        paddingTop: 30,
        paddingHorizontal: 10
    },
    overallInfo: {
        flexDirection: 'column',
    },
    locationAndMembers: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    budgetAnDeposit: {
        marginTop: 10
    },
    expense: {
        marginTop: 30
    },
    itemList: {
        marginTop: 10
    },
    amount: {
        fontWeight: 'bold',
        color: colors.primary,
        fontSize: 18
    }
});

const mapStateToProps = (state, ownProps) => {
    const sheetId = ownProps.navigation.getParam('sheetId', -1);
    return {
        sheet: _.find(state.sheets.sheets, item => item._id === sheetId),
        isLoading: state.sheets.fetching
    }
};

export default connect(mapStateToProps)(SheetDetailScreen);