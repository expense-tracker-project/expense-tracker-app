import React from "react"
import {
    FlatList,
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity
} from "react-native"
import Budget from "../components/Budget";
import AppText from '../components/AppText';
import EditBudgetModal from "../components/EditBudgetModal"
import { isEmpty } from "../common/utils/ArrayUtils"
import { PUG_GIF } from "../images"
import AppHeader from '../components/AppHeader';
import { TITLE_BUDGET_SCREEN } from '../constants/title_screen';
import MyIcon from '../config/icon-font';
import colors from '../constants/colors';

export default class BudgetScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedBudget: {},
            budgets: [
                {
                    id: 1,
                    amount: 20
                },
                {
                    id: 2,
                    amount: 30
                },
                {
                    id: 3,
                    amount: 15
                },
                {
                    id: 4,
                    amount: 35
                }
            ],
            showEditBudgetModal: false
        }
    }

    openEditBudgetModal = (budget) => {
        this.setState({
            selectedBudget: budget,
            showEditBudgetModal: true
        })
    }

    closeEditBudgetModal = () => {
        this.setState({ showEditBudgetModal: false })
    }

    saveBudget = (budget) => {
        this.setState(previousState => {
            const budgetIndex = previousState.budgets.findIndex(value => value.id === budget.id)
            budget.id = previousState.budgets.length + 1
            if (budgetIndex === -1) {
                // If budget has not been added yet, add it
                return {
                    budgets: previousState.budgets.concat(budget),
                }
            } else {
                // Else update current one
                return {
                    budgets: previousState.budgets.map((value, index) => {
                        if (index === budgetIndex) {
                            return budget
                        } else {
                            return value
                        }
                    }),
                }
            }
        })
        this.closeEditBudgetModal()
    }

    removeBudget = (budget) => {
        this.setState(previousState => {
            return {
                budgets: previousState.budgets.filter(item => item.id !== budget.id),
            }
        })
    }

    calculateTotalBudget = (budgets) => {
        if (isEmpty(budgets)) {
            return 0;
        } else {
            return budgets.map(budget => budget.amount).reduce((accumulator, currentValue) => accumulator + currentValue)
        }
    }

    renderEmptyBudget() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                <View style={style.emptyBudgetContainer}>
                    <Image source={PUG_GIF} style={style.emptyBudgetImage} />
                    <AppText style={style.emptyBudgetText}>No Pugget added</AppText>
                    {this.renderEditBudgetModal()}
                </View>
            </View>
        )
    }

    renderHeader = () => {
        return (
            <AppHeader
                hasBackButton={true}
                navigation={this.props.navigation}
                rightIcons={
                    <TouchableOpacity onPress={this.openEditBudgetModal}>
                        <MyIcon
                            name={'plus'}
                            size={20}
                            color={colors.white} />
                    </TouchableOpacity>
                }
                title={TITLE_BUDGET_SCREEN}
            />
        )
    }

    renderBudgets() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                <View>
                    <FlatList
                        data={this.state.budgets}
                        renderItem={({ item }) =>
                            <Budget
                                budget={item}
                                onSelect={this.openEditBudgetModal}
                                onRemove={this.removeBudget}
                            />
                        }
                        keyExtractor={(item, index) => item + index}
                    />
                    <View style={style.totalAmountContainer}>
                        <AppText style={style.totalAmountTitle}>Total</AppText>
                        <AppText style={style.totalAmountValue}>{this.calculateTotalBudget(this.state.budgets)}e</AppText>
                    </View>
                    {this.renderEditBudgetModal()}
                </View>
            </View>
        )
    }

    renderEditBudgetModal() {
        return (
            <EditBudgetModal
                style={{ backgroundColor: 'red' }}
                budget={this.state.selectedBudget}
                isVisible={this.state.showEditBudgetModal}
                onSave={this.saveBudget}
                onClose={this.closeEditBudgetModal}
            />
        )
    }

    render() {
        if (isEmpty(this.state.budgets)) {
            return this.renderEmptyBudget()
        } else {
            return this.renderBudgets()
        }
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    totalAmountContainer: {
        alignItems: 'flex-end',
        marginTop: 8,
        marginRight: 16
    },
    totalAmountTitle: {
        fontSize: 25
    },
    totalAmountValue: {
        fontSize: 20
    },
    emptyBudgetContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyBudgetImage: {
        width: 150,
        height: 150
    },
    emptyBudgetText: {
        fontSize: 18
    }
})
