import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import Expense from "./Expense";

const ExpenseList = (props) => {
    const { expenses, onItemPress } = props;

    return(
        <FlatList
            data={expenses}
            keyExtractor={item => item._id}
            renderItem={ ({item}) => {
                return (
                    <Expense
                        expense={item}
                        onItemPress={onItemPress}
                    />
                );
            }}
        />
    )
};

ExpenseList.propTypes = {
    expenses: PropTypes.array.isRequired,
    onItemPress: PropTypes.func
};

ExpenseList.defaultProps = {
    expenses: []
};

export default ExpenseList
