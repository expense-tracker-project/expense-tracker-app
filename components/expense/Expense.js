import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from "prop-types";
import AppText from "../AppText";
import colors from "../../constants/colors";

const Expense = (props) => {
    const { expense, onItemPress } = props;

    return (
        <TouchableOpacity style={styles.container} onPress={() => onItemPress && onItemPress(expense._id)}>
            <View style={styles.infoContainer}>
                <AppText>{expense.name}</AppText>
                {/*<AppText style={styles.lightGreyText}>{`Paid by ${expense.name}`}</AppText>*/}
            </View>
            <View style={styles.amountContainer}>
                <AppText style={styles.amount}>{`${'\u20AC'}${expense.cost}`}</AppText>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        minHeight: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        marginTop: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 3,
        margin: 10,
        marginVertical: 5
    },
    infoContainer: {
        flexDirection: 'column'
    },
    amountContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    amount: {
        fontWeight: 'bold',
        color: colors.primary,
        fontSize: 18
    }
});

Expense.propTypes = {
    expense: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        cost: PropTypes.number
    }).isRequired,
    onItemPress: PropTypes.func
};

export default Expense
