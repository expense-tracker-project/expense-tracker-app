import React from 'react';
import { View, StyleSheet } from 'react-native';
import AppText from "../components/AppText";
import AppTextInput from "./AppTextInput";
import {Button} from 'react-native-elements';

export default class UserProfile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newEmail: props.user.email,
            newUsername: props.user.username,
        }
    }

    render() {
        const {user,isEditing,toggleEdit,editProfile} = this.props
        const {email, username} = user
        const {newEmail, newUsername} = this.state
        return (
            <View>
                <View style={styles.mainSection}>
                    <AppText style={styles.sectionHeader}>Account information</AppText>
                    <View style={styles.infoContainer}>
                        <View style={isEditing ? styles.infoEditing : styles.info}>
                            <AppText style={styles.textStyle}>Email</AppText>
                            {isEditing ?
                                <AppTextInput
                                    defaultValue={email}
                                    style={styles.userInput}
                                    clearButtonMode="while-editing"
                                    onChangeText={newEmail => this.setState({newEmail})}
                                /> :
                                <AppText style={styles.textStyle}>{email}</AppText>
                            }
                        </View>
                        {isEditing ?
                            <View style={isEditing ? styles.infoEditing : styles.info}>
                                <AppText style={styles.textStyle}>New password</AppText>
                                <AppTextInput
                                    style={styles.userInput}
                                    clearButtonMode="while-editing"
                                    secureTextEntry={true}
                                />
                                <AppText style={styles.textStyle}>Confirm new password</AppText>
                                <AppTextInput
                                    style={styles.userInput}
                                    clearButtonMode="while-editing"
                                    secureTextEntry={true}
                                />
                            </View> :
                            <View style={isEditing ? styles.infoEditing : styles.info}>
                                <AppText style={styles.textStyle}>Password</AppText>
                                <AppText style={styles.textStyle}>&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;</AppText>
                            </View>
                        }
                        <View style={isEditing ? styles.infoEditing : styles.info}>
                            <AppText style={styles.textStyle}>Username</AppText>
                            {isEditing ?
                                <AppTextInput
                                    defaultValue={username}
                                    style={styles.userInput}
                                    clearButtonMode="while-editing"
                                    onChangeText={newUsername => this.setState({newUsername})}
                                /> :
                                <AppText style={styles.textStyle}>{user.username}</AppText>
                            }
                        </View>
                    </View>
                </View>

                <View style={styles.mainSection}>
                    <AppText style={styles.sectionHeader}>Sheet information</AppText>
                    <View style={styles.infoContainer}>
                        <View style={styles.info}>
                            <AppText style={styles.textStyle}>Host</AppText>
                            <AppText style={styles.textStyle}>3</AppText>
                        </View>
                        <View style={styles.info}>
                            <AppText style={styles.textStyle}>Member</AppText>
                            <AppText style={styles.textStyle}>9</AppText>
                        </View>
                    </View>
                </View>

                {isEditing &&
                <View style={{flexDirection: 'row'}}>
                    <Button
                        title="Save"
                        containerViewStyle={styles.buttonContainer}
                        buttonStyle={styles.button}
                        textStyle={styles.buttonTitle}
                        onPress={() => editProfile(newEmail,newUsername)}
                    />
                    <Button
                        title="Cancel"
                        containerViewStyle={styles.buttonContainer}
                        buttonStyle={styles.button}
                        textStyle={styles.buttonTitle}
                        onPress={toggleEdit}
                    />
                </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: "#FFF",
        fontSize: 18
    },
    mainSection: {
        marginTop: 10
    },
    sectionHeader: {
        fontSize: 20,
        color: "#FFF"
    },
    infoContainer: {
        backgroundColor: 'rgb(166,127,142)',
        borderRadius: 10,
        padding: 15,
    },
    infoEditing: {
       marginTop: 10
    },
    info: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    userInput:{
        marginTop: 5,
        width: '100%',
        fontFamily: 'Avenir',
        fontSize: 16,
        color: "#FFF",
        borderBottomWidth: 1,
        borderBottomColor: "#FFF"
    },
    buttonContainer: {
        flex:1,
        marginTop: 20,
        marginBottom: 20,
        alignItems: "center",
    },
    buttonTitle: {
        fontSize: 18,
        fontFamily: "Avenir",
        fontWeight: "800",
        color: "#fff"
    },
    button: {
        width: 130,
        padding: 7,
        backgroundColor: 'rgb(166,127,142)',
        borderRadius: 15,
    },
})
