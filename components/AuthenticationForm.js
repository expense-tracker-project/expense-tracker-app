import React from 'react';
import {StyleSheet, View } from 'react-native';
import AppText from '../components/AppText';
import SignupForm from './SignupForm';
import LoginForm from './LoginForm';
import {toggleAuthFormAction} from '../actions/ui-action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class AuthenticationForm extends React.Component {
    render() {
        const { onNavigation, showLoginForm, toggleAuthFormAction } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.switchForm}>
                    <View style={showLoginForm ? styles.authButtonActive : styles.authButtonInactive}>
                        <AppText
                            style={styles.authButton}
                            onPress={() => toggleAuthFormAction(true)}
                        >
                            Login
                        </AppText>
                    </View>
                    <AppText style={styles.authButton}>
                        &#160;&#160; &#124; &#160;&#160;
                    </AppText>
                    <View style={showLoginForm ? styles.authButtonInactive : styles.authButtonActive}>
                        <AppText
                            style={styles.authButton}
                            onPress={() => toggleAuthFormAction(false)}
                        >
                            Signup
                        </AppText>
                    </View>
                </View>
                <View style={styles.userInputArea}>
                    {showLoginForm ?
                        <LoginForm onNavigation={onNavigation}/>
                        :
                        <SignupForm onNavigation={onNavigation}/>
                    }

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(255, 255, 255, 0.4)",
        justifyContent: "center",
        padding: 20,
        paddingTop: 10,
        paddingBottom: 30,
        width: 300,
        borderRadius: 10,
    },
    switchForm: {
        flexDirection: "row",
        justifyContent: "flex-start",
        padding: 5,
        margin: 5,
        marginTop: 0,
    },
    authButton: {
        fontSize: 18,
        fontWeight: "800",
        color: "#ED6A5A",
        marginBottom: 0
    },
    authButtonActive: {
        borderBottomWidth: 2,
        borderColor: "#ED6A5A"
    },
    authButtonInactive: {
        borderWidth: 0,
    },
    userInputArea: {
        marginTop: 20,
    }
});

const mapStateToProps = (store) => {
    return {
        showLoginForm: store.ui.showLoginForm
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleAuthFormAction: bindActionCreators(toggleAuthFormAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationForm);

