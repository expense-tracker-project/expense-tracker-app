import React from 'react';
import { StyleSheet, View } from 'react-native';
import BasicListItem from './BasicListItem';
import { Avatar } from 'react-native-elements';
import colors from '../constants/colors';
import ProgressBar from 'react-native-progress/Bar';
import AppText from './AppText';

const PayerDetailListItem = (props) => {
    const { name, amount, returned } = props;
    const content = () => {
        return (
            <View style={styles.itemContent}>
                <View style={styles.metaText}>
                    <View style={styles.info}>
                        <Avatar
                            small
                            rounded
                            source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}}
                            activeOpacity={0.7}/> 
                        <AppText style={[styles.username, styles.darkGrey]}> {name} </AppText>
                    </View>
                    <View>
                        <AppText style={styles.darkGrey}> {`${'\u20AC'}${amount}`} </AppText>
                    </View>
                </View>
                <View style={styles.progressBar}>
                    <AppText style={[styles.amount, styles.darkGrey]}> {`Received amount: ${'\u20AC'}${returned} / ${'\u20AC'}${amount}`}</AppText>
                    <ProgressBar 
                        progress={returned / amount} 
                        width={null} 
                        height={8}
                        borderRadius={8}
                        color={colors.primary}
                        borderColor={colors.darkGrey}/>
                </View>
            </View>
        )
    }

    return (
        <BasicListItem 
            left={content()}
        />
    )
}

const styles = StyleSheet.create({
    metaText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    username: {
        marginHorizontal: 10
    },
    progressBar: {
        marginTop: 10
    },
    amount: {
        marginBottom: 10
    },
    darkGrey: {
        color: colors.darkGrey
    }
})

export default PayerDetailListItem
