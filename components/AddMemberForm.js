import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { StyleSheet, View, TextInput, Text } from "react-native";
import { Button } from 'react-native-elements';
import colors from '../constants/colors';
import AppText from './AppText';

export default class AddMemberForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            amountTextInputValue: '',
            weightTextInputValue: ''
        }
    }

    clearForm = () => {;
        this.amountTextInput.clear();
        if (this.weightTextInput) {
            this.weightTextInput.clear();
        }
    }

    render() {
        const { type, memberData } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.block}>
                    <AppText style={styles.title}> MEMBER </AppText>
                    <Dropdown
                        label='Select member'
                        data={memberData}
                        baseColor={colors.darkGrey}
                        itemColor={colors.darkGrey}
                        dropdownPosition={-3}
                        itemTextStyle={styles.pickerText}
                        labelTextStyle={styles.pickerText}
                        baseTextStyle={styles.pickerText}
                        ref={ref => this.dropDown = ref}
                        value={this.state.dropdownValue}
                        onChangeText={dropdownValue => this.setState({ dropdownValue })}
                    />
                </View>
                <View style={styles.block}>
                    <AppText style={styles.title}>AMOUNT</AppText>
                    <TextInput
                        style={styles.textInput}
                        keyboardType="number-pad"
                        value={this.state.amountTextInputValue}
                        onChangeText={(amountTextInputValue) => this.setState({ amountTextInputValue })}
                        ref={ref => this.amountTextInput = ref} />
                </View>
                {
                    type === 'Returned' &&
                    <View style={styles.block}>
                        <AppText style={styles.title}>WEIGHT</AppText>
                        <TextInput
                            style={styles.textInput}
                            keyboardType="number-pad"
                            value={this.state.weightTextInputValue}
                            onChangeText={(weightTextInputValue) => this.setState({ weightTextInputValue })}
                            ref={ref => this.weightTextInput = ref} />
                    </View>
                }
                <View style={styles.buttonGroup}>
                    <Button
                        title="SAVE"
                        containerViewStyle={styles.buttonContainer}
                        buttonStyle={[styles.button, styles.save]}
                        textStyle={styles.buttonTitle}
                    />
                    <Button
                        title="CANCEL"
                        containerViewStyle={styles.buttonContainer}
                        buttonStyle={[styles.button, styles.cancel]}
                        textStyle={styles.buttonTitle}
                        onPress={this.clearForm}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {
        marginVertical: 10
    },
    title: {
        fontWeight: 'bold',
        color: colors.darkGrey,
        fontSize: 17,
        marginBottom: 7
    },
    textInput: {
        borderBottomWidth: 0.5,
        color: colors.darkGrey,
        fontSize: 17
    },
    buttonGroup: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    buttonContainer: {
        flex: 1
    },
    button: {
        borderRadius: 5,
        marginVertical: 5
    },
    buttonTitle: {
        fontSize: 17,
        color: colors.white
    },
    save: {
        backgroundColor: colors.primary
    }
})

