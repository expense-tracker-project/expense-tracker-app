import React from 'react';
import { View, StyleSheet } from 'react-native';
import {randomImage} from "../common/utils/ImageUtils";
import {Avatar} from 'react-native-elements';
import AppText from "../components/AppText";
import AppTextInput from "./AppTextInput";
import { Dropdown } from 'react-native-material-dropdown';
import {Button} from 'react-native-elements';

export default class MemberInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newRole: props.member.role,
            newNickname: props.member.nickname
        }
    }

    render() {
        const {member, isEditing, editMember, toggleEditMember} = this.props;
        const {id, username, nickname, role} = member;
        const {newRole, newNickname} = this.state;

        return (
            <View>
                <View style={styles.avatar}>
                    <Avatar
                        xlarge
                        rounded
                        source={randomImage()}
                        activeOpacity={0.7}
                    />
                    <AppText style={styles.username}>{username}</AppText>
                </View>

                <View style={styles.profileInfo}>
                    <View style={styles.info}>
                        <AppText style={styles.header}>Nickname</AppText>
                        {isEditing ?
                            <AppTextInput
                                defaultValue={nickname}
                                style={styles.userInput}
                                clearButtonMode="while-editing"
                                onChangeText={newNickname => this.setState({newNickname})}
                            /> :
                            <AppText>{nickname}</AppText>
                        }
                    </View>
                </View>

                <View style={styles.profileInfo}>
                    <View style={styles.info}>
                        <AppText style={styles.header}>Role</AppText>
                        {isEditing ?
                            <Dropdown
                                label='Choose member role'
                                data={[{value: 'Host'}, {value: 'Member'}]}
                                pickerStyle={{borderWidth: 1, borderRadius: 5}}
                                baseColor="#000"
                                itemColor="#000"
                                dropdownPosition={-3}
                                value={role}
                                onChangeText={newRole => this.setState({newRole})}
                            /> : <AppText>{role}</AppText>}

                    </View>
                </View>
                {isEditing &&
                    <View style={{flexDirection: 'row'}}>
                        <Button
                            title="Save"
                            containerViewStyle={styles.buttonContainer}
                            buttonStyle={styles.button}
                            textStyle={styles.buttonTitle}
                            onPress={() => editMember(newNickname, newRole, id)}
                        />
                        <Button
                            title="Cancel"
                            containerViewStyle={styles.buttonContainer}
                            buttonStyle={styles.button}
                            textStyle={styles.buttonTitle}
                            onPress={toggleEditMember}
                        />
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({

    avatar: {
        alignItems: 'center',
        marginTop: 10
    },
    username: {
        fontSize: 30,
        margin: 10,
    },
    header: {
        fontSize: 18,
        fontWeight: '500'
    },
    profileInfo: {
        margin: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    info: {
        flex:1,
    },
    userInput:{
        marginTop: 10,
        width: '100%',
        fontFamily: 'Avenir',
        borderBottomWidth: 0.5,
    },
    buttonContainer: {
        marginTop: 20,
        marginBottom: 20,
        alignItems: "center",
    },
    buttonTitle: {
        fontSize: 18,
        fontFamily: "Avenir",
        fontWeight: "800",
        color: "#fff"
    },
    button: {
        width: 130,
        padding: 7,
        backgroundColor: "#ED6A5A",
        borderRadius: 15,
    },
});
