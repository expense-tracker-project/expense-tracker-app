import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {StyleSheet, View} from 'react-native';
import PropTypes from 'prop-types'

const IconButton = (props) => {
    const {onPress, name, size, color} = props
    return (
        <View style={[styles.container, props.style]}>
            <Icon onPress={onPress} name={name} size={size} color={color}/>
        </View>
    )
}

IconButton.propTypes = {
    onPress: PropTypes.func,
    name: PropTypes.string.isRequired,
    size: PropTypes.number,
    color: PropTypes.string
}

IconButton.defaultProps = {
    size: 24,
    color: "#000"
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        margin: 10
    }
});

export default IconButton


