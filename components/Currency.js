import React from 'react';
import {StyleSheet, View} from "react-native"
import AppText from "./AppText"
import Flag from 'react-native-flags'
import PropTypes from 'prop-types'

const Currency = (props) => {
    const { currency } = props;
    const { code,name} = currency;
    return (
        <View style={styles.currencyContainer}>
            <Flag code={code} type='flat' size={48} style={styles.flag}/>
            <AppText style={styles.currencyName}>{name}</AppText>
        </View>
    )
}



Currency.propTypes = {
    currency: PropTypes.object.isRequired
}
Currency.defaultProps = {
    checked: false,
}

const styles = StyleSheet.create({
    currencyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    flag: {
        marginRight: 20
    },
    currencyName: {
        marginTop: 20,
        marginBottom: 20
    }
});
export default Currency




