import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

const BasicListItem = (props) => {

    const { left, right } = props;

    return (
        <TouchableOpacity 
            style={styles.container}
            onPress={props.onExpensePress}>
            <View style={styles.left}>{left}</View>
            { right && <View style={styles.amountContainer}>{right}</View>}
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    container: {
        minHeight: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        marginTop: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 3,
        margin: 10,
        marginVertical: 5
    },
    left: {
        flex: 1
    },
    amountContainer: {
        flex: 1,
        alignItems: 'flex-end'
    }

})

export default BasicListItem
