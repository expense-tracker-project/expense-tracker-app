import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Avatar, Card } from 'react-native-elements'
import Swipeout from "react-native-swipeout"
import { randomImage } from "../common/utils/ImageUtils"
import EvilIcons from "react-native-vector-icons/EvilIcons";
import colors from '../constants/colors';
import MyIcon from '../config/icon-font';

const Budget = (props) => {
    const { budget, onSelect, onRemove } = props;
    const { amount } = budget;

    const swipeOutButtons = [
        {
            component:
                <View style={style.swipeOutButton}>
                    <MyIcon
                        name={'garbage'}
                        size={20}
                        color={colors.white} />
                </View>,
            onPress: () => onRemove(budget)
        }
    ]

    return (
        <Card containerStyle={style.card}>
            <Swipeout
                style={style.swipeOutContainer}
                autoClose={true}
                right={swipeOutButtons}
            >
                <TouchableOpacity
                    style={style.contentContainer}
                    onPress={() => onSelect(budget)}
                >
                    <View style={style.memberContainer}>
                        <Avatar
                            rounded
                            title='tt'
                            source={randomImage()}
                            activeOpacity={0.7}
                            containerStyle={style.avatarContainer}
                        />
                        <Text>Kidsukezzz</Text>
                    </View>
                    <View style={style.amountContainer}>
                        <Text>{amount}e</Text>
                    </View>
                </TouchableOpacity>
            </Swipeout>
        </Card>
    )
}

const style = StyleSheet.create({
    card: {
        borderWidth: 0,
        borderRadius: 8,
        elevation: 4,
        marginBottom: 6,
        padding: 0
    },
    swipeOutContainer: {
        borderRadius: 8,
        backgroundColor: 'white'
    },
    swipeOutButton:{
        flex: 1, 
        backgroundColor: colors.primary, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 12
    },
    memberContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    avatarContainer: {
        marginRight: 8
    },
    amountContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export default Budget
