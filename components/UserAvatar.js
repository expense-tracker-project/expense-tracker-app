import React from "react"
import AppText from "../components/AppText";
import {StyleSheet,View} from "react-native";
import {randomImage} from "../common/utils/ImageUtils";
import {Avatar} from 'react-native-elements';
import {AVATAR_1} from '../images';

const UserAvatar = (props) => {
    const {username, mode, memberType, style, usernameStyle, userTypeStyle} = props
    return (
        <View style={[mode == 'vertical' ? styles.avatarVertical : styles.avatarHorizontal, style]}>
          {mode == 'vertical' ?
              <Avatar
                  xlarge
                  rounded
                  source={randomImage()}
                  activeOpacity={0.7}
              /> :
              <Avatar
                  large
                  rounded
                  source={randomImage()}
                  activeOpacity={0.7}
              />
          }
            <View style={styles.userInfo}>
                <AppText style={[styles.username, usernameStyle]}>{username}</AppText>
                {mode == 'horizontal' && <AppText style={userTypeStyle}>{memberType}</AppText>}
            </View>
        </View>
    )
}

UserAvatar.defaultProps = {
    mode: 'vertical'
}

const styles = StyleSheet.create({
    avatarVertical: {
        alignItems: 'center',
        marginTop:20,
    },
    avatarHorizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft:20,
    },
    userInfo: {
        marginLeft: 20
    },
    username: {
        fontSize: 25,
        marginTop: 15,
        color: '#FFF'
    },
})

export default UserAvatar
