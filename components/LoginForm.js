import React from 'react';
import {StyleSheet, View, TextInput, Image} from 'react-native';
import {Button} from 'react-native-elements';
import AppText from './AppText';
import colors from '../constants/colors';
import { FACEBOOK_LOGO, GOOGLE_LOGO, TWITTER_LOGO } from "../images";
import AppTextInput from './AppTextInput';
import FBLoginButton from "./FBLoginButton"

class LoginForm extends React.Component {

    render() {
        const { onNavigation } = this.props;
        return (
            <View style={styles.container}>
                <AppTextInput
                    style={styles.userInput}
                    placeholder="Email address"
                    placeholderTextColor={colors['placeholder']}
                />
                <AppTextInput
                    secureTextEntry={true}
                    style={styles.userInput}
                    placeholder="Password"
                    placeholderTextColor={colors['placeholder']}
                />
                <Button
                    title="Log in"
                    containerViewStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    textStyle={styles.buttonTitle}
                    onPress={() => onNavigation('Home')}
                />
                <AppText style={styles.text}>OR</AppText>
                <View style={styles.socialLogoContainer}>
                    <FBLoginButton style={styles.socialLogo}/>
                    <Image source={GOOGLE_LOGO} style={styles.dumpSocialLogo}/>
                    <Image source={TWITTER_LOGO} style={styles.dumpSocialLogo}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "column"
    },
    userInput:{
        backgroundColor: "#fff",
        height: 35,
        paddingLeft: 15,
        marginBottom: 15,
        fontFamily: 'Avenir',
        borderRadius: 15
    },
    buttonContainer: {
        marginTop: 20,
        marginBottom: 20,
        alignItems: "center",
    },
    buttonTitle: {
        fontSize: 18,
        fontFamily: "Avenir",
        fontWeight: "800",
        color: "#fff"
    },
    button: {
        width: 130,
        padding: 7,
        backgroundColor: "#ED6A5A",
        borderRadius: 18,
    },
    text: {
      textAlign: "center",
      fontWeight: "900"
    },
    socialLogoContainer: {
        flexDirection: "row",
        justifyContent: "center"
    },
    dumpSocialLogo: {
        width: 32,
        height: 32,
        margin: 5,
        marginTop: 20,
    },
    socialLogo: {
        margin: 5,
        marginTop: 20
    }
});

export default LoginForm;