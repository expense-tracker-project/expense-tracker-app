import React from 'react'
import PropTypes from 'prop-types'
import {TouchableOpacity, View, StyleSheet} from 'react-native'
import {Avatar, Card} from 'react-native-elements'
import { randomImage } from '../../common/utils/ImageUtils'
import { SimpleLineIcons } from "@expo/vector-icons"
import AppText from "../AppText"
import moment from 'moment'

const Sheet = (props) => {
    const { sheet, mode, onItemPress } = props;
    const { _id, name, createdAt, lastModifiedAt, users } = sheet;

    return (
        <TouchableOpacity style={style.wrapper} onPress={() => onItemPress && onItemPress(_id)}>
            <Card containerStyle={style.card}>
                <View style={style.container}>
                    <SimpleLineIcons style={style.icon} name='rocket' size={26} color='black' />
                    <View>
                        <View style={style.content}>
                            <AppText>{name}</AppText>
                            <AppText>{createdAt}</AppText>
                        </View>
                        <View style={style.footer}>
                            {
                                mode === 'normal' &&
                                users.map((user, index) => {
                                    const offset = index * -8;
                                    return (
                                        <Avatar
                                            rounded
                                            title={user.displayName}
                                            source={randomImage()}
                                            activeOpacity={0.7}
                                            containerStyle={{position: 'relative', left: offset,  zIndex: -index}}
                                            key={user._id}
                                        />
                                    )
                                })
                            }
                            {
                                mode === 'recently' &&
                                <AppText>Edited {moment(lastModifiedAt, 'MMMM Do YYYY, h:mm:ss a').fromNow()} by longv</AppText>
                            }
                        </View>
                    </View>
                </View>
            </Card>
        </TouchableOpacity>
    )
};

const style = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    card: {
        flex: 1,
        flexDirection: 'row',
        borderWidth: 0,
        borderRadius: 8,
        elevation: 4,
        marginBottom: 6,
        marginHorizontal: 10
    },
    emptyBudgetContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    content: {},
    icon: {
        marginTop: 8,
        marginRight: 16,
    },
    footer: {
        flexDirection: 'row',
        marginTop: 16
    }
});

Sheet.propTypes = {
    sheet: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        createdAt: PropTypes.string,
        lastModifiedAt: PropTypes.string,
        users: PropTypes.array
    }).isRequired,
    onItemPress: PropTypes.func,
    mode: PropTypes.string
};

Sheet.defaultProps = {
    mode: 'normal'
};

export default Sheet
