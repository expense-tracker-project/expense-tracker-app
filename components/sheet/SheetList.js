import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import Sheet from "./Sheet";

const SheetList = (props) => {
    const { sheets, mode, horizontal, style, onItemPress } = props;

    return(
        <FlatList
            style={[style]}
            data={sheets}
            horizontal={horizontal}
            keyExtractor={item => item._id}
            renderItem={ ({item}) => {
                return (
                    <Sheet
                        sheet={item}
                        mode={mode}
                        onItemPress={onItemPress}
                    />
                );
            }}
        />
    )
};

SheetList.propTypes = {
    sheets: PropTypes.array.isRequired,
    style: PropTypes.object,
    mode: PropTypes.string,
    horizontal: PropTypes.bool,
    onItemPress: PropTypes.func
};

SheetList.defaultProps = {
    sheets: [],
    horizontal: false,
    mode: 'normal'
};

export default SheetList
