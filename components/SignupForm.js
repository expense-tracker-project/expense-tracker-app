import React from 'react';
import {StyleSheet, View, Alert } from 'react-native';
import {Button} from 'react-native-elements';
import colors from '../constants/colors';
import validator from "../common/utils/validator";
import AppTextInput from './AppTextInput';
import {bindActionCreators} from 'redux';
import {userSignupAction} from '../actions/users-action';
import {connect} from 'react-redux';
import {BarIndicator} from 'react-native-indicators';

class SignupForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            usernameError:'',
            email: '',
            emailError: '',
            password: '',
            passwordError: '',
            confirmPassword: '',
            confirmPasswordError: ''
        }
    }

    userSignup = () => {
        const {username, email, password, confirmPassword} = this.state
        const usernameError = validator('username', username)
        const emailError = validator('email', email)
        const passwordError = validator('password', password)
        const confirmPasswordError = validator('confirmPassword', confirmPassword, 'password', password);

        this.setState({emailError, passwordError, confirmPasswordError, usernameError});

        if (!usernameError && !emailError && !passwordError && !confirmPasswordError) {
            const {userSignupAction} = this.props;
            userSignupAction(email, password, username)
        }
    }

    signupSucceeded = () => {
        const {onNavigation, user} = this.props
        Alert.alert(
            'Success',
            'Signup successfully',
            [
                {text: 'OK', onPress: () => onNavigation('Home', {user})},
            ],
            { cancelable: false }
        )
    }

    signupFailed = (msg) => {
        Alert.alert(
            'Error',
            `${msg}`,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }


    render() {
        const {username, email, password, confirmPassword, usernameError, emailError, passwordError, confirmPasswordError} = this.state;
        const { isLoading, user, errorMessage } = this.props;

        if (errorMessage) {
            this.signupFailed(errorMessage)
        }
        if (user) {
            this.signupSucceeded()
        }

        return (
            <View style={styles.container}>
                {isLoading && <BarIndicator count={6} color='white'/>}
                <AppTextInput
                    style={styles.userInput}
                    placeholder="Username"
                    placeholderTextColor={colors['placeholder']}
                    onChangeText={username => this.setState({username})}
                    onBlur={() => {
                        this.setState({
                            usernameError: validator('username', username)
                        })
                    }}
                    error={usernameError}
                />
                <AppTextInput
                    style={styles.userInput}
                    placeholder="Email address"
                    placeholderTextColor={colors['placeholder']}
                    onChangeText={email => this.setState({email})}
                    onBlur={() => {
                        this.setState({
                            emailError: validator('email', email)
                        })
                    }}
                    error={emailError}
                />
                <AppTextInput
                    secureTextEntry={true}
                    style={styles.userInput}
                    placeholder="Password"
                    placeholderTextColor={colors['placeholder']}
                    onChangeText={password => this.setState({password})}
                    onBlur={() => {
                        this.setState({
                            passwordError: validator('password', password)
                        })
                    }}
                    error={passwordError}
                />
                <AppTextInput
                    secureTextEntry={true}
                    style={styles.userInput}
                    placeholder="Confirm password"
                    placeholderTextColor={colors['placeholder']}
                    onChangeText={confirmPassword => this.setState({confirmPassword})}
                    onBlur={() => {
                        this.setState({
                            confirmPasswordError: validator('confirmPassword', confirmPassword, 'password', password)
                        })
                    }}
                    error={confirmPasswordError}
                />

                <Button
                    title="Sign up"
                    containerViewStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    textStyle={styles.buttonTitle}
                    onPress={this.userSignup}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
    },
    userInput:{
        backgroundColor: "#fff",
        height: 35,
        paddingLeft: 15,
        marginBottom: 15,
        fontFamily: 'Avenir',
        borderRadius: 15
    },
    buttonContainer: {
        marginTop: 20,
        alignItems: "center",
    },
    buttonTitle: {
        fontSize: 18,
        fontFamily: "Avenir",
        fontWeight: "800",
        color: "#fff"
    },
    button: {
        width: 130,
        padding: 7,
        backgroundColor: "#ED6A5A",
        borderRadius: 18,
    }

});

const mapStateToProps = (store) => {
    return {
        user: store.usersState.user,
        isLoading: store.usersState.isLoading,
        errorMessage: store.usersState.errorMessage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userSignupAction: bindActionCreators(userSignupAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);

