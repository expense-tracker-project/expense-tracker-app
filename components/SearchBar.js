import React from 'react';
import {StyleSheet, View, TextInput} from "react-native"
import Icon from 'react-native-vector-icons/Feather';
import AppTextInput from './AppTextInput';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: ''
        }
    }

    handleOnTextChange = (searchString) => {
        const {handleSearching} = this.props;
        handleSearching(searchString);
        this.setState({searchString});
    }

    render() {
        const {searchString} = this.state;
        return (
            <View style={styles.searchBar}>
                <Icon name="search" size={14} color="#AFAFAF" style={styles.searchIcon}/>
                <AppTextInput
                    containerStyle={styles.input}
                    clearTextOnFocus={true}
                    clearButtonMode="while-editing"
                    placeholder="Search for currency"
                    onChangeText={searchString => this.handleOnTextChange(searchString)}
                    onEndEditing={() => this.setState({searchString: ''})}
                    value={searchString}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    searchBar: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 0.2,
        borderColor: '#BBBBBB',
        backgroundColor: '#F1F1F1',
        padding: 5,
    },
    searchIcon: {
        margin: 5
    },
    input: {
        flex: 1,
    }
});
export default SearchBar;




