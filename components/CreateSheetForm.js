import React, { Component } from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import {StyleSheet,View, TextInput,TouchableOpacity} from "react-native";
import AppText from "./AppText";
import RadioGroup from 'react-native-radio-buttons-group';
import {Button} from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import colors from '../constants/colors';

class CreateSheetForm extends Component {

    render() {
        const
            {
                sheetType,
                sheetCategory,
                isDatePickerVisible,
                toggleDateTimePicker,
                handleSheetTypePicked,
                handleDatePicked,
                pickedDueDate
            } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <AppText style={styles.inputLabel}>Sheet's name</AppText>
                    <TextInput style={styles.textInput}/>
                </View>

                <View style={styles.inputContainer}>
                    <View style={styles.dropDownArea}>
                        <Dropdown
                            label='Sheet category'
                            labelFontSize={18}
                            data={sheetCategory}
                            pickerStyle={{borderWidth: 1, borderRadius: 5}}
                            baseColor="#000"
                            itemColor="#000"
                            dropdownPosition={-3}
                            itemTextStyle={styles.pickerText}
                            labelTextStyle={styles.pickerText}
                            baseTextStyle={styles.pickerText}
                        />
                    </View>

                    <View style={styles.radioButtons}>
                        <RadioGroup
                            radioButtons={sheetType}
                            onPress={handleSheetTypePicked}
                            flexDirection='row'
                        />
                    </View>
                </View>

                <View style={styles.inputContainer}>
                    <AppText style={styles.inputLabel}>Due date (tap to set)</AppText>
                    <TouchableOpacity onPress={toggleDateTimePicker}>
                        <AppText style={styles.pickedDate}>{pickedDueDate}</AppText>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={isDatePickerVisible}
                        onConfirm={handleDatePicked}
                        onCancel={toggleDateTimePicker}
                    />
                </View>

                <View style={styles.inputContainer}>
                    <AppText style={styles.inputLabel}>Budget</AppText>
                    <TextInput style={styles.textInput} keyboardType="number-pad"/>
                </View>

                <Button
                    title="Create"
                    containerViewStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    textStyle={styles.buttonTitle}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        justifyContent: "center",
    },
    dropDownArea: {
        marginTop: 15
    },
    pickerText: {
        fontFamily: 'Avenir',
        fontSize:18
    },
    radioButtons: {
        marginTop: 15
    },
    inputContainer: {
      marginBottom: 10
    },
    inputLabel: {
        fontWeight: '400',
        fontSize: 18,
        marginBottom: 20,
        marginTop: 20
    },
    textInput: {
        borderBottomWidth: 0.5,
    },
    pickedDate: {
        marginTop: 0,
        textAlign: 'center',
    },
    buttonContainer: {
        alignItems: "center",
        marginTop: 20,
    },
    buttonTitle: {
        fontFamily: "Avenir",
        fontWeight: "700",
        fontSize: 17,
        color: colors.white
    },
    button: {
        backgroundColor: colors.primary,
        borderRadius: 5,
        marginVertical: 5,
        width: 150
    }
})

export default CreateSheetForm;
