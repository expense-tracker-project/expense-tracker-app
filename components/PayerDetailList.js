import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import PayerDetailListItem from './PayerDetailListItem';
import expenseData from '../fixtures/expenseData.json';
import colors from '../constants/colors';

const PayerDetailList = (props) => {
    const { expense } = props;
    return (
        <FlatList 
            style={styles.list}
            data={expense.payers}
            keyExtractor={payers => payers.id.toString()}
            renderItem={ ({item}) => {
                return <PayerDetailListItem {...item} />
            }}
        />
    );
}

const styles = StyleSheet.create({
    list: {
        marginVertical: 10
    }
})

export default PayerDetailList
