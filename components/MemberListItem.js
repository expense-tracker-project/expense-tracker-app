import React from 'react';
import {StyleSheet,View, TouchableOpacity} from "react-native";
import AppText from "../components/AppText";
import {Avatar} from 'react-native-elements'
import {randomImage} from "../common/utils/ImageUtils";


export default class MemberListItem extends React.Component {


    onPress = () => {
        const {member, onPress, editMember, removeMember} = this.props;
        onPress('MemberDetailScreen', {member, 'editMember': editMember.bind(this), 'removeMember': removeMember.bind(this)})
    }

    render() {
        const {member} = this.props
        const {username, nickname} = member
        return (
            <TouchableOpacity onPress={this.onPress}>
                <View style={styles.container}>
                    <Avatar
                        medium
                        rounded
                        source={randomImage()}
                        activeOpacity={0.7}
                    />
                    <View style={styles.memberInfo}>
                        <AppText>{nickname}</AppText>
                        <AppText style={styles.username}>{username}</AppText>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: "center",
        flexDirection: 'row',
        marginBottom: 10,
    },
    memberInfo: {
        flex: 1,
        marginLeft: 20,
    },
    username: {
        fontSize: 12
    }

});
