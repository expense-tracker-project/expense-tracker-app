import React from 'react';
import {Image, StyleSheet, TouchableOpacity} from 'react-native'
import {FACEBOOK_LOGO} from "../images"

export default class FBLoginButton extends React.Component {

    render() {
        return (
            <TouchableOpacity
                style={this.props.style}
                onPress={this.loginFacebook}>
                <Image source={FACEBOOK_LOGO} style={styles.icon}/>
            </TouchableOpacity>
        );
    }

    loginFacebook = async () => {
        const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('714626228904096', {
            permissions: ['public_profile'],
        });
        if (type === 'success') {
            const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
            console.log(await response.json())
        } else {
            console.log("Failed")
        }
    }

};

const styles = StyleSheet.create({
    icon: {
        width: 32,
        height: 32
    }
});