import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, TextInput, Text} from 'react-native'
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Feather';
import {Dropdown} from "react-native-material-dropdown"
import {Button} from "react-native-elements"
import Ripple from "react-native-material-ripple"

export default class EditBudgetModal extends React.Component {
    constructor(props) {
        super(props)

        const { budget } = props

        this.state = {
            budget: budget,
            member: budget.member,
            amount: budget.amount
        }
    }

    componentWillReceiveProps(nextProps) {
        const { budget } = nextProps

        this.setState({
            budget: budget,
            member: budget.member,
            amount: budget.amount
        })
    }

    saveBudget = () => {
        const { onSave } = this.props

        if (onSave) {
            const { budget } = this.props
            const { amount } = this.state
            budget.amount = amount
            onSave(budget)
        }
    }

    render() {
        const { onClose, isVisible} = this.props;
        const { budget, amount } = this.state

        const isValid = (this.state.amount) &&
            (this.state.amount !== this.state.budget.amount)

        return (
            <Modal isVisible={isVisible}
                   onBackButtonPress={onClose}
                   onBackdropPress={onClose}
            >
                <View style={styles.modalContent}>
                    <View style={styles.headerContainer}>
                        <Text style={styles.title}>{budget.id ? "Edit" : "Add"} budget</Text>
                        <Ripple
                            rippleCentered={true}
                            onPress={onClose}
                        >
                            <Icon name="x" size={24} color="#000"/>
                        </Ripple>
                    </View>
                    <View style={styles.contentContainer}>
                        <Dropdown
                            label='Choose member'
                            data={[{value: 'hello'}]}
                            baseColor="#000"
                            itemColor="#000"
                            dropdownPosition={-2}
                        />
                        <TextInput
                            style={styles.amountInput}
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                            defaultValue={amount ? amount.toString() : ""}
                            onChangeText={(text) =>
                                this.setState({ amount: parseInt(text) })
                            }
                        />
                    </View>
                    <View style={styles.footerContainer}>
                        <Button
                            title="OK"
                            color={'#26A69A'}
                            fontWeight="500"
                            disabled={!isValid}
                            disabledTextStyle={{color: "#9E9E9E"}}
                            disabledStyle={{backgroundColor: '#FFF'}}
                            containerViewStyle={{marginTop: 8, marginRight: 0}}
                            buttonStyle={{backgroundColor: '#FFF'}}
                            onPress={this.saveBudget}
                        />
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modalContent: {
        padding: 16,
        backgroundColor: "white",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    contentContainer: {
        flexDirection: 'column',
        justifyContent:'flex-start'
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    title: {
        flex: 1,
        fontSize: 20,
        fontWeight: "600"
    },
    amountInput: {
        height: 40,
        borderColor: '#000',
        borderBottomWidth: 1
    }
});

EditBudgetModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    isVisible: PropTypes.bool.isRequired,
    budget: PropTypes.object.isRequired
}

EditBudgetModal.defaultProps = {
    isVisible: false,
    budget: {}
}
