import React from 'react';
import { View, Switch,StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import colors from '../constants/colors';
import BasicListItem from './BasicListItem';
import AppText from './AppText';

export default ReturnerBasicList = (props) => {
    const { returner } = props;
    return (
        <BasicListItem 
            left={leftComponent(returner)}
            right={rightComponent(returner)}
        />
    )
}

const leftComponent = (returner) => {
    return (
        <View style={styles.leftComponent}>
            <Avatar
                small
                rounded
                source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}}
                activeOpacity={0.7}/> 
                <View style={styles.metaText}>
                    <AppText>{returner.nickName}</AppText>
                    <AppText style={styles.weight}>{`Weight: ${returner.weight}`}</AppText>
                </View>
        </View>
    )
}

const rightComponent = (returner) => {
    return (
        <View style={styles.rightComponent}>
            <AppText style={styles.amount}>{`${'\u20AC'}${returner.amount}`}</AppText>
            <Switch 
                onTintColor={colors.primary}
                value={returner.hasReturned}
                onValueChange={(value) => { 
                    returner.hasReturned = value;
                    }} />
        </View>
    )
}

const styles = StyleSheet.create({
    list: {
        marginTop: 10
    },
    leftComponent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rightComponent: {
        alignItems: 'flex-end'
    },
    metaText: {
        marginLeft: 10
    },
    weight: {
        marginTop: 5,
        color: colors.lightGrey
    },
    amount: {
        fontWeight: 'bold',
        color: colors.darkGrey,
        marginBottom: 5
    }
})