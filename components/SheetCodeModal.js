import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View } from 'react-native';
import AppTextInput from '../components/AppTextInput';
import AppText from '../components/AppText';
import IconButton from './IconButton';
import Modal from 'react-native-modal';

const SheetCodeModal = (props) => {
    const {modalVisible, setModalVisible} = props;
    return (
        <Modal isVisible={modalVisible} onBackdropPress={setModalVisible}>
            <View style={styles.modalContent}>
                <IconButton name="x" onPress={setModalVisible}/>
                <View style={styles.codeInputContainer}>
                    <AppText style={styles.codeInputText}>Enter sheet code</AppText>
                    <AppTextInput
                        style={styles.codeInput}
                        autoCapitalize="characters"
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContent: {
        flexDirection: 'column',
        backgroundColor: "white",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        margin: 10
    },
    closingButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        margin: 10
    },
    codeInputContainer: {
        flexDirection: 'column',
        justifyContent:'flex-start',
        margin: 25,
        marginTop: 0
    },
    codeInputText: {
        fontSize: 20,
        fontWeight: "600",
    },
    codeInput: {
        borderColor: '#000',
        borderBottomWidth: 1
    }
});

SheetCodeModal.propTypes = {
    modalVisible: PropTypes.bool.isRequired,
    setModalVisible: PropTypes.func.isRequired
}

export default SheetCodeModal;
