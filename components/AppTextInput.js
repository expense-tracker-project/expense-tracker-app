import React, { Component } from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import AppText from './AppText';

export default class AppTextInput extends Component {
    render() {
        return (
            <View style={this.props.containerStyle}>
                <TextInput
                    {...this.props}
                    underlineColorAndroid="transparent"
                />
                {this.props.error ? <AppText style={styles.errorText}>{this.props.error}</AppText> : null}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    errorText: {
        color: 'red',
        fontSize: 12,
        marginBottom: 10
    }
});