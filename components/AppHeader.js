import React from 'react';
import { View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { LinearGradient } from 'expo';
import MyIcon from './../config/icon-font.js';
import colors from '../constants/colors';
import AppText from './AppText';

const screenHeight = Dimensions.get("window").height;
const HEADER_HEIGHT_RATIO = 0.08;

const Header = (props) => {
    const { title, navigation, hasBackButton, rightIcons } = props;
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={[colors.primary, colors.secondary]}
                start={[0, -0.2]}
                end={[0, 1.5]}
                style={styles.header}>

                <TouchableOpacity
                    style={styles.backButton}
                    onPress={() => navigation.goBack(null)}>
                    { hasBackButton &&
                        <MyIcon
                            name={'left-arrow'}
                            size={20}
                            color={colors.white} />
                    }
                </TouchableOpacity>
                <AppText style={styles.title}> {title} </AppText>
                <View style={styles.headerRight}>
                    { rightIcons }
                </View>
            </LinearGradient>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: screenHeight * HEADER_HEIGHT_RATIO
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingHorizontal: 10
    },
    title: {
        color: colors.white,
        marginBottom: 0,
        textAlign: 'center',
        fontWeight: 'bold',
        flex: 2
    },
    backButton: {
        flex: 1
    },
    headerRight: {
        flex: 1,
        alignItems: 'flex-end'
    }
})

export default Header;