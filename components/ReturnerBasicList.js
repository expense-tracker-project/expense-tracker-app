import React from 'react';
import { 
    View, 
    Switch, 
    FlatList, 
    StyleSheet 
} from 'react-native';
import { Avatar } from 'react-native-elements';
import colors from '../constants/colors';
import ReturnerListItem from './ReturnerListItem';

const returners = [
    {
        name: "phuch",
        nickName: "co be hay quyt",
        weight: 1,
        hasReturned: true,
        amount: 6 
    },
    {
        name: "longvh",
        nickName: "cau be hay quyt",
        weight: 2,
        hasReturned: false,
        amount: 12 
    },
    {
        name: "tring",
        nickName: "em be hay quyt",
        weight: 0,
        hasReturned: true,
        amount: 0 
    },
    {
        name: "khap",
        nickName: "ba trum hay quyt",
        weight: 1,
        hasReturned: true,
        amount: 6 
    }
];

const ReturnerBasicList = (props) => {
    return (
        <FlatList
            style={styles.list}
            data={returners}
            keyExtractor={returner => returner.name}
            renderItem={ ({item}) => <ReturnerListItem returner={item} /> }
        />
    )
}

const styles = StyleSheet.create({
    list: {
        marginTop: 10
    },
    leftComponent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rightComponent: {
        alignItems: 'flex-end'
    },
    metaText: {
        marginLeft: 10
    },
    weight: {
        marginTop: 5,
        color: colors.lightGrey
    },
    amount: {
        fontWeight: 'bold',
        color: colors.darkGrey,
        marginBottom: 5
    }
})

export default ReturnerBasicList