import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { LinearGradient } from 'expo';
import MyIcon from './../config/icon-font.js';
import colors from '../constants/colors';
import AppText from './AppText';

const screenHeight = Dimensions.get("window").height;
const HEADER_HEIGHT_RATIO = 0.15;

const SheetDetailHeader = (props) => {

    const { handleNavigation, popAction } = props;

    return (
        <View style={styles.container}>
            <LinearGradient
                colors={[colors.primary, colors.secondary]}
                start={[0, -0.2]}
                end={[0, 1.5]}
                style={styles.gradientBackground}>
                <View style={styles.content}>
                    <View style={styles.icons}>
                        <TouchableOpacity onPress={popAction}>
                            <MyIcon
                                name={'left-arrow'}
                                size={20}
                                color={colors.white} />
                        </TouchableOpacity>
                        <View style={styles.rightIcons}>
                            <TouchableOpacity>
                                <MyIcon
                                    style={styles.marginRight}
                                    name={'connections'}
                                    size={20}
                                    color={colors.white} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => handleNavigation('SettingSheetDetail', {})}>
                                <MyIcon
                                    name={'settings'}
                                    size={20}
                                    color={colors.white} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.title}>
                        <AppText style={styles.titleText}>Nuksio party</AppText>
                    </View>
                </View>
            </LinearGradient>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: screenHeight * HEADER_HEIGHT_RATIO
    },
    gradientBackground: {
        height: '100%'
    },
    content: {
        paddingTop: 20,
        paddingBottom: 0,
        paddingHorizontal: 15,
        flex: 1,
        borderRadius: 5,
        flexDirection: 'column'
    },
    icons: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        flex: 1,
        justifyContent: 'center',
    },
    titleText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    rightIcons: {
        flexDirection: 'row'
    },
    marginRight: {
        marginRight: 20
    }
})

export default SheetDetailHeader
